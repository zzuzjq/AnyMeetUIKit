//
//  AMTopToolView.m
//  AnyMeetUIKit
//
//  Created by derek on 2018/4/28.
//  Copyright © 2018年 derek. All rights reserved.
//

#import "AMTopToolView.h"
#import "AMCommon.h"

@implementation AMTopToolView
+ (instancetype)loadTopToolView {
    return [[[NSBundle mainBundle]loadNibNamed:@"AMTopToolView" owner:self options:nil] lastObject];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.titleLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    self.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    
    [self.leaveButton setTitleColor:[AMCommon getColor:@"#FF4141"] forState:UIControlStateNormal];
    self.leaveButton.titleLabel.font = [UIFont systemFontOfSize:16];
    

    
}
- (IBAction)doSomeEvent:(id)sender {
    UIButton *button = (UIButton*)sender;
    button.selected = !button.selected;
    switch (button.tag) {
        case 200:
        {
            //扬声器
            if (self.speakerBlock) {
                self.speakerBlock(button.selected);
            }
        }
            break;
        case 201:
        {
            if (self.leaveBlock) {
                self.leaveBlock();
            }
        }
            break;
        case 202:
        {
            if (self.switchBlock) {
                self.switchBlock();
            }
        }
            break;
        default:
            break;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
