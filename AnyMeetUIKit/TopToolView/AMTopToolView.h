//
//  AMTopToolView.h
//  AnyMeetUIKit
//
//  Created by derek on 2018/4/28.
//  Copyright © 2018年 derek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMTopToolView : UIView
@property (weak, nonatomic) IBOutlet UIButton *switchCameraButton;
//离开
@property (weak, nonatomic) IBOutlet UIButton *leaveButton;
//标题
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//扬声器
@property (weak, nonatomic) IBOutlet UIButton *spearkerButton;

typedef void(^LeaveBlock)(void);
typedef void(^SpeakerBlock)(BOOL isOn);
typedef void(^SwitchBlock)(void);

@property (nonatomic, copy) LeaveBlock leaveBlock;
@property (nonatomic, copy) SpeakerBlock speakerBlock;
@property (nonatomic, copy) SwitchBlock switchBlock;

+ (instancetype)loadTopToolView;


@end
